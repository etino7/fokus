const axios = require("axios");

export default class PhotoGenerator {

  constructor(){
    let rawdata = require("./apiKey.json")  
      this.key = rawdata.key;
      this.randomNum = Math.floor(Math.random() * 72);  
  }

  getRandomPhoto() {
    return axios.get(`https://api.unsplash.com/collections/8875153/photos?client_id=${this.key}&page=${this.randomNum}&per_page=1`)
  }
}
