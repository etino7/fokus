# Fokus
Fokus is a pomodoro timer web app. Gain control over your time and up your productivity...

![alt text](/gitassets/fokusexmpl.JPG "Fokus screen demo")

## Demo 
You can try Fokus now at https://fokus.etino.dev/

## About
Fokus was built as a side project while learning and playing with vuejs. Wallpapers are fetched from Unsplash.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```
